# Composer template for FPU website projects

This project template should provide a kickstart for creating new websites for
Fresno Pacific University and managing your site dependencies with 
[Composer](https://getcomposer.org/).

If you want to know how to use it as replacement for
[Drush Make](https://github.com/drush-ops/drush/blob/master/docs/make.md) visit
the [Documentation on drupal.org](https://www.drupal.org/node/2471553).

## Usage

First you need to [install composer](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx).

> Note: The instructions below refer to the [global composer installation](https://getcomposer.org/doc/00-intro.md#globally).
You might need to replace `composer` with `php composer.phar` (or similar) 
for your setup.

* Clone this project and run `composer install`
* If no errors occur, navigate in your browser to `https://yourwebsite.com` and
follow the steps to finalize the creation of your website
* Delete the existing `.git` directory in your composer root
* Run `git init;git add -A;git commit -m 'Initial commit'` 
* Setup a new repository for your new website on BitBucket

### IMPORTANT: Before your first commit...

After your website is created, move the database credentials that were added to
the bottom of the `settings.php` file to `settings.local.php` BEFORE making any
git commits.


With `composer require ...` you can download new dependencies to your 
installation.

```
cd project-root
composer require drupal/devel:~1.0
```

## What does the template do?

When installing the given `composer.json` some tasks are taken care of:

* Drupal will be installed in the `docroot`-directory.
* Autoloader is implemented to use the generated composer autoloader in `vendor/autoload.php`,
  instead of the one provided by Drupal (`docroot/vendor/autoload.php`).
* Modules (packages of type `drupal-module`) will be placed in `docroot/modules/contrib/`
* Theme (packages of type `drupal-theme`) will be placed in `docroot/themes/contrib/`
* Profiles (packages of type `drupal-profile`) will be placed in `docroot/profiles/contrib/`
* Creates default writable versions of `settings.php` and `services.yml`.
* Creates default writable versions of `settings.local.php` and `services.yml`.
* Sets the `config/` directory in `settings.php`
* Creates `docroot/sites/default/files`-directory.
* Latest version of drush is installed locally for use at `vendor/bin/drush`.
* Latest version of DrupalConsole is installed locally for use at `vendor/bin/drupal`.

## Updating Drupal Core

This project will attempt to keep all of your Drupal Core files up-to-date; the 
project [drupal-composer/drupal-scaffold](https://github.com/drupal-composer/drupal-scaffold) 
is used to ensure that your scaffold files are updated every time drupal/core is 
updated. If you customize any of the "scaffolding" files (commonly .htaccess), 
you may need to merge conflicts if any of your modified files are updated in a 
new release of Drupal core.

Follow the steps below to update your core files.

1. Run `composer update drupal/core --with-dependencies` to update Drupal Core and its dependencies.
1. Run `git diff` to determine if any of the scaffolding files have changed. 
   Review the files for any changes and restore any customizations to 
  `.htaccess` or `robots.txt`.
1. Commit everything all together in a single commit, so `docroot` will remain in
   sync with the `core` when checking out branches or running `git bisect`.
1. In the event that there are non-trivial conflicts in step 2, you may wish 
   to perform these steps on a branch, and use `git merge` to combine the 
   updated core files with your customized files. This facilitates the use 
   of a [three-way merge tool such as kdiff3](http://www.gitshah.com/2010/12/how-to-setup-kdiff-as-diff-tool-for-git.html). This setup is not necessary if your changes are simple; 
   keeping all of your modifications at the beginning or end of the file is a 
   good strategy to keep merges easy.

## Updating Contrib Modules

1. Run `composer update drupal/module-name --with-dependencies`

## Using Lando for local development

* Install Docker and [Lando](https://docs.devwithlando.io/installation/installing.html) on your local machine.
* Clone the fpubase8-project repository from Bitbucket to your local machine.
* Navigate to your fpubase8-project project directory that contains the .lando.yml file and run `lando start`.
* Navigate to `docroot/sites/default`, copy the settings.lando.php file, and save a copy of the file named `settings.local.php`.
* Navigate to https://fpubase8.lndo.site/core/install.php and run the site install process in the browser.
* The site should be accessible at [https://fpubase8.lndo.site](https://fpubase8.lndo.site).

## FAQ

### How can I apply patches to downloaded modules?

If you need to apply patches (depending on the project being modified, a pull 
request is often a better solution), you can do so with the 
[composer-patches](https://github.com/cweagans/composer-patches) plugin.

To add a patch to drupal module foobar insert the patches section in the extra 
section of composer.json:
```json
"extra": {
    "patches": {
        "drupal/foobar": {
            "Patch description": "URL to patch"
        }
    }
}
```
